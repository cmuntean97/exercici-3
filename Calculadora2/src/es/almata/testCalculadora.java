package es.almata;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class testCalculadora {

	//test suma
	@ParameterizedTest
    @CsvSource({"10,5,15",
   			    "13,3,16",
   			    "79,2,81",
   			    "5,4,9",
   			    "33,7,40"})
    @DisplayName("A + B = C")
    void sumaParamArray(int a, int b, int resultat) {
   	 Calculadora c = new Calculadora();
   	 assertEquals(resultat, c.suma(a, b));
    }

	
	//test resta
	@ParameterizedTest
    @CsvSource({"27,6,21",
   			    "70,3,67",
   			    "9,4,5",
   			    "100,54,46",
   			    "16,2,14"})
    @DisplayName("A - B = C")
    void restaParamArray(int a, int b, int resultat) {
   	 Calculadora c = new Calculadora();
   	 assertEquals(resultat, c.resta(a, b));
    }
	
	
	//test multiplicacio
	@ParameterizedTest
    @CsvSource({"7,7,49",
   			    "5,3,15",
   			    "6,4,24",
   			    "2,10,20",
   			    "33,7,231"})
    @DisplayName("A * B = C")
    void multiplicacioParamArray(int a, int b, int resultat) {
   	 Calculadora c = new Calculadora();
   	 assertEquals(resultat, c.multiplicacio(a, b));
    }
	
	
	//test divisio
	@ParameterizedTest
    @CsvSource({"80,4,20",
   			    "100,10,10",
   			    "6,2,3",
   			    "90,3,30",
   			    "30,5,6"})
    @DisplayName("A / B = C")
    void divisioParamArray(int a, int b, int resultat) {
   	 Calculadora c = new Calculadora();
   	 assertEquals(resultat, c.divisio(a, b));
    }
		
		
	@AfterAll
    public static void afterAll() {
   	 System.out.println("Els tests han acabat.");
    }

}
